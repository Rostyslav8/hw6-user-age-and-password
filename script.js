
function createNewUser() {
  return {
    firstName: prompt("Enter your name?"),
    lastName: prompt("Enter your surname?"),
    userBirthday: prompt("Enter you birthday DD.MM.YYYY"),

    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase()
    },

    getAge() {
      let currentYear = new Date().getFullYear()
      let currentMonth = new Date().getMonth()
      let currentDay = new Date().getDate()
      const birthDay = +this.userBirthday.slice(0, 2)
      const birthMonth = +this.userBirthday.slice(3, 5)
      const birthYear = +this.userBirthday.slice(6, 10)
      let age = currentYear - birthYear
      if (birthMonth > currentMonth + 1 || (birthMonth === currentMonth + 1 && birthDay > currentDay)) {
        age--
      }

      return age
    },

    getPassword() {
      return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.userBirthday.substring(6, 10)
    },
  }
};

const newUser = createNewUser();
console.log("Age: ", newUser.getAge());
console.log("Password: ", newUser.getPassword());